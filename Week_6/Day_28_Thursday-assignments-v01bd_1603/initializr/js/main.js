function create_disk_container(id ,width) {
  var disk_container = document.createElement('div');
  disk_container.id = id + 1;
  disk_container.style.width = width + "px";
  disk_container.style.height = "20px";
  disk_container.style.top = 100 + 22 * id + "px";
  disk_container.style.left = "0px";
  disk_container.style.position = "absolute";
  document.getElementById("main").appendChild(disk_container);
  create_disk(id);
}

function create_disk(size) {
  var disk = document.createElement('div');
  disk.style.width = 20 + 15 * size + "px";
  disk.style.height = 20 + "px";
  document.getElementById(size + 1).appendChild(disk);
}


function animation(counter,disk ,pow_2 ,stack_size ,disk_pos ,from ,to ,main_height ,num_itr) {
  if(counter[1]) {
    i = counter[0];
    solver(i,disk ,pow_2 ,stack_size ,disk_pos ,from ,to);
    counter[1] =0;
  }
  //document.write(to[0]);document.write("<br>");
  var disk1 = document.getElementById(disk[0]);
  
  finaly = main_height - 22 * stack_size[to[0] -1];
  //document.write(disk[0]);
  //from=[1];to=[2];
  transition(disk1 ,from ,to ,finaly ,num_itr);
  
}

function solver(i ,disk ,pow_2 ,stack_size ,disk_pos ,from ,to) {
  num_pow = pow_2.length;
  last_pow = pow_2[num_pow - 1];
  if(i == last_pow) {
    disk[0] = num_pow + 1;
    last_pow *=2;
    pow_2.push(last_pow);
  } else {
    for(j=0 ;j < num_pow ;j++) {
      if(i % pow_2[j] !== 0) {
        disk[0] = j + 1;
        //document.write(i);document.write(pow_2[j]);document.write("<br>");
        break;
      }
    }
  }
  from[0] = disk_pos[disk[0] - 1 ];
  to[0] = from[0] + 1 + (disk[0] + 1) % 2;
  if ( to[0] > 3) {
    to[0] = to[0] % 3;
  }
  disk_pos[disk[0] - 1 ] = to[0];
  stack_size[from[0] -1] -= 1;
  stack_size[to[0] -1] += 1;
  //document.write(from[0]+"=>");document.write(to[0]);document.write(disk[0]);document.write("<br>");
}

function transition(disk ,from ,to ,finaly ,num_itr) {
  width = disk.style.width;
  width = parseInt(width);
  y = disk.style.top;
  y = parseInt(y ,10);
  x= disk.style.left;
  x = parseInt(x ,10);
  finalx = (to[0]-1) * width;
  if (y != 50 &&  x != finalx) {
    y--;
    disk.style.top = y + "px";
    return 0;
  } else if(x != finalx) {
    horizontal(from ,to ,x ,disk);
    return 0 ;
  }
  if (y != finaly) {
    y++;
    disk.style.top = y + "px";
    return 0 ;
  }
  
  counter[0]++;
  counter[1] = 1;
  if (counter[0] == num_itr) {
    clearInterval(id);
  }
  
}

function horizontal(from ,to ,current_pos ,disk) {
  distance = to[0] - from[0] ;
  increment = distance / Math.abs(distance);
  disk.style.left = increment + current_pos + "px";
}

var num_disks = prompt("enter number of disks");
main_width = 20 + 15 * num_disks;
main_height = 100 + 22 * num_disks ;
var main_container = document.createElement('div');
main_container.id = "main";
main_container.style.width = main_width * 3 + "px";
main_container.style.height = main_height + "px";
document.body.appendChild(main_container);
for(x = 0 ;x < num_disks ;x++) {
  create_disk_container(x ,main_width);
}

/**
 * position of disks in each stak
 * ex : disk 5 in stak 2 => disk_pos[5] = 2
**/
var disk_pos = [];
var counter = [1,1];
//number of moves
var disk = [1];
var num_itr = Math.pow(2, num_disks);
//cointains the power of ex : 2,4,8...
var pow_2 = [2];
var stack_size = [num_disks ,0 ,0];
for(x = 0; x < num_disks; x++) {
  disk_pos.push(1);
}
var from = [0];
var to = [0];
var id = setInterval(function(){animation(counter,disk ,pow_2 ,stack_size ,disk_pos ,from ,to ,main_height ,num_itr)}, 10);

  
//myMove();