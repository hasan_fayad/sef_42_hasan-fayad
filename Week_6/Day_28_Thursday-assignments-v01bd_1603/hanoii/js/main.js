function main(main_width ,main_height) {
  var main_container = document.createElement('div');
  main_container.id = "main";
  main_container.style.width = main_width * 3 + "px";
  main_container.style.height = main_height + "px";
  document.body.appendChild(main_container);
}

function create_disk_container(id ,width) {
  var disk_container = document.createElement('div');
  disk_container.id = id + 1;
  disk_container.style.width = width + "px";
  disk_container.style.height = "20px";
  disk_container.style.top = 100 + 22 * id + "px";
  disk_container.style.left = "0px";
  disk_container.style.position = "absolute";
  document.getElementById("main").appendChild(disk_container);
  create_disk(id);
}

function create_disk(size) {
  var disk = document.createElement('div');
  disk.style.width = 20 + 15 * size + "px";
  disk.style.height = 20 + "px";
  document.getElementById(size + 1).appendChild(disk);
}

//function solve should be 3 seperate functions
function solver(itirator ,coord) {
  num_pow = itirator.pow_2.length;
  last_pow = itirator.pow_2[num_pow - 1];
  //check for which disk to move
  if(itirator.i == last_pow) {
    itirator.disk = num_pow + 1;
    last_pow *=2;
    itirator.pow_2.push(last_pow);
  } else {
    for(j=0 ;j < num_pow ;j++) {
      if(itirator.i % itirator.pow_2[j] !== 0) {
        itirator.disk = j + 1;
        //document.write(i);document.write(pow_2[j]);document.write("<br>");
        break;
      }
    }
  }
  //find from and to (from rodd 1 to 2 )
  itirator.from = itirator.disk_pos[itirator.disk - 1 ];
  itirator.to = itirator.from + 1 + (itirator.disk + 1) % 2;
  if ( itirator.to > 3) {
    itirator.to = itirator.to % 3;
  }
  itirator.disk_pos[itirator.disk - 1 ] = itirator.to;
  itirator.stack_size[itirator.from -1] -= 1;
  itirator.stack_size[itirator.to -1] += 1;
  //find the coordinate and last coordinate of disk
  coord.disk = document.getElementById(itirator.disk);
  width = coord.disk.style.width;
  width = parseInt(width);
  coord.finalx = (itirator.to-1) * width;
  coord.finaly = main_height - 22 * itirator.stack_size[itirator.to -1];
  y = coord.disk.style.top;
  coord.y = parseInt(y ,10);
  x= coord.disk.style.left;
  coord.x = parseInt(x ,10);
  
}

function transition(itirator ,coord ) {
  //up
  if (coord.y != 50 &&  coord.x != coord.finalx) {
    coord.y--;
    coord.disk.style.top = coord.y + "px";
    return 0;
    //right or left
  } else if(coord.x != coord.finalx) {
    distance = itirator.to - itirator.from;
    increment = distance / Math.abs(distance);
    coord.x += increment;
    coord.disk.style.left = coord.x + "px";
    return 0 ;
  }
  //down
  if (coord.y != coord.finaly) {
    coord.y++;
    coord.disk.style.top = coord.y + "px";
    return 0 ;
  }
  
  itirator.i++;
  if(itirator.stack_size[itirator.to -1] == num_disks) {
    clearInterval(id);
    return 0;
  }
  solver(itirator ,coord);
}

var num_disks = prompt("enter number of disks");
main_width = 20 + 15 * num_disks;
main_height = 100 + 22 * num_disks ;
main(main_width ,main_height);

for(x = 0 ;x < num_disks ;x++) {
  create_disk_container(x ,main_width);
}

/**
 * position of disks in each stak
 * ex : disk 5 in stak 2 => disk_pos[5] = 2
**/
var itirator = {
  disk_pos : [],
  i : 1,
  disk : 1,
  pow_2 : [2],
  stack_size : [num_disks ,0 ,0],
  from : 0,
  to : 0
};
var coord = {
  disk : 0,
  x : 0,
  y : 0,
  finalx : 0,
  finaly : 0
};
for(x = 0; x < num_disks; x++) {
  itirator.disk_pos.push(1);
}
solver(itirator ,coord);
var id = setInterval(function(){transition(itirator ,coord)}, 1);

