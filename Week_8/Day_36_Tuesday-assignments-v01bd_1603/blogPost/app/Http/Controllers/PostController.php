<?php

namespace blogPost\Http\Controllers;

use Illuminate\Http\Request;

use blogPost\Http\Requests;

use blogPost\Post;

use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function post($id)
    {
        $post = Post::where('id', $id)->first();

        return view('blog.post')->with('post', $post);
    }

    public function addpost(Request $request) {
		$post = new Post() ;
		$post->title = $request->get('title');
		$post->text = $request->get('text');
		$post->user_id = $request->user()->id;
        $post->user_name = Auth::user()->name;
		$post->save();
		return redirect('/');
    }

    public function deletepost($id) {
    	$post = Post::find($id);
		$post->delete();
		return redirect()->back();
    }

    public function editpostform($id) {
    	$post = Post::where('id', $id)->first();

    	return view('blog.edit')->with('post', $post);
    }

    public function editpost(Request $request) {

    	$post = Post::find($request->get('id'));
		$post->text = $request->get('text');
		$post->title = $request->get('title');
		$post->user_id = $request->user()->id;
		$post->save();
		return redirect($request->get('referrer'));
    }

    public function addpostform() {
        return view('blog.add');
    }
}
