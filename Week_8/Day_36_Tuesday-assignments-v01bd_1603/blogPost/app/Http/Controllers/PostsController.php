<?php

namespace blogPost\Http\Controllers;

use Illuminate\Http\Request;

use blogPost\Http\Requests;

use blogPost\Post;

use Illuminate\Support\Facades\Auth;

class PostsController extends Controller
{
	 public function posts()
    {
        $posts = Post::orderBy('updated_at', 'desc')
                ->paginate(10);

        return view('blog.posts', compact('posts'));
    }

    public function myposts()
    {
        $userId = Auth::id();

        $posts = Post::where('user_id', $userId)
                    ->orderBy('updated_at', 'desc')
        			->paginate(10);

        //return $userId;
        return view('blog.posts', compact('posts'));
    }
    //
}
