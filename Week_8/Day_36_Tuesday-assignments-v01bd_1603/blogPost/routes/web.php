<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'PostsController@posts');

Auth::routes();

Route::get('/home', function() {
	return redirect('/');
});

Route::get('/blog/{id}', 'PostController@post');


Route::group(['middleware' => ['auth']], function()
{
	Route::get('/myposts', 'PostsController@myposts');

	Route::get('/delete/{id}', 'PostController@deletepost');

	Route::post('/add', 'PostController@addpost');

	Route::post('/edit/{id}', 'PostController@editpost');

	Route::get('/add', 'PostController@addpostform');

	Route::get('/edit/{id}', 'PostController@editpostform');
 
});