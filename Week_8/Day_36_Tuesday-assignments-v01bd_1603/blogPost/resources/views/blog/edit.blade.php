@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Add Post</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/edit/'.$post->id) }}">
                        {{ csrf_field() }}
                        <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-1 control-label">Title</label>

                            <div class="col-md-10">
                                <input id="title" value="{{ $post->title }}" type="text" class="form-control" name="title"  required autofocus>
                                <input id="title" value="{{ $post->id }}" type="hidden" name="id"  >
                                <input  value="{{ Request::server('HTTP_REFERER') }}" type="hidden" name="referrer"  >

                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-1 control-label">Text</label>

                            <div class="col-md-10">
                                <textarea id="text"  rows="5" class="form-control" name="text" required>{{ $post->text }}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-5">
                                <button type="submit" class="btn btn-primary">
                                    Edit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

