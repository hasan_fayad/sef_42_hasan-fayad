@extends('layouts.app')

@section('content')
@foreach ($posts as $post)
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @if(Auth::id() == $post->user_id)
                        <a href="{{ url('delete/'.$post->id) }}">delete : </a>
                        @endif
                        <a href="{{ url('blog/'.$post->id) }}">{{ $post->title }}</a>
                    </div>

                    <div class="panel-body">
                        {{ str_limit($post->text) }} 
                        <br>author : {{ $post->user_name }}
                        @if($post->created_at != $post->updated_at)
                            <br>(edited)
                        @endif
                        @if(Auth::id() == $post->user_id)
                            <br><a href="{{ url('edit/'.$post->id) }}">edit</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach
<div class="text-center">
    <ul class="pagination">
        <li>{{ $posts->links() }}</li> 
    </ul>
</div>
@endsection
