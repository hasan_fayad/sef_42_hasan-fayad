<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
  /**
   * Run the database seeds.
   */
  public function run()
  {
    Model::unguard();

    $this->call('PostTableSeeder');
  }

}

class PostTableSeeder extends Seeder
{
  public function run()
  {
    blogPost\Post::truncate();

    factory(blogPost\Post::class, 20)->create();
  }
}
