<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(blogPost\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(blogPost\Post::class, function (Faker\Generator $faker) {
  return [
    'title' => $faker->sentence(1),
    'text' => join("\n\n", $faker->paragraphs(1)),
    'user_id' => random_int(1, 3),
  ];
});
