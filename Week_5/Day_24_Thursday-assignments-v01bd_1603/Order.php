<?php
require_once("MySQLWrap.php");
require_once("Config.php");

function post_handler($post) {
	$handler=array(array());
	foreach($post as $name =>$value) {
		if($value) {
			$index = explode(",",$name);
			$handler[$index[0]][$value] = 1;
		}
	} 
	return $handler;
}

function check_box($name,$value) {
	echo '<input type= "checkbox" name= "'
		.$name.'" value= "'.$value.'" checked>';
}

function drop_down($name,$sqlwrap) {
	echo $name;
	echo '   <select name = "'.$name.'">';
	echo '<option ></option>';
	$columns = array($name."_id","name");
	if ($name == "film") {
		$columns[1] = "title";
	} elseif ($name == "actor") {
		$columns[1] = 'concat(first_name,
			"  ",last_name)';
	} 
	$query = $sqlwrap->select_query($name,$columns);
	$results = $sqlwrap->exec_query($query);
	foreach ($results as $result) {
		echo '<option value="'.$result[$columns[0]].
			'|'.$result[$columns[1]].'">'.
			$result[$columns[1]].'</option>';
	}
	echo '</select><br>';
}

function get_conditons($post) {
	$conditions=array(array());
	foreach($post as $name =>$value) {
		if($value) {
			$index = explode(",",$name);
			$id = explode("|",$value)[0];
			if(!isset($conditions[$index[0]][0])) {
				$conditions[$index[0]][0] = $id;
			} else {
				array_push($conditions[$index[0]],$id);
			}
		}
	} 
	unset($conditions[0]);
	return $conditions;
}

function film_rule($films_id) {
	arsort($films_id);
	$max = 0;
	$rule = "";
	foreach($films_id as $film_id => $count) {
		if (!$max) {
			$rule = " where film_id = ".$film_id;
			$max = $count;
		} elseif($count == $max) {
			$rule .= " or film_id = ".$film_id;
		}
	}
	return $rule;
}

function films($sqlwrap ,$rule) {
	$query = $sqlwrap->select_query("film".$rule,
			array('title' ,'film_id'));
	$results = $sqlwrap->exec_query($query);
	return $results;
}

$attributes = array("actor","category","language");
$sqlwrap = new MySQLWrap();
$sqlwrap->connect($host, $user, $pass, $db);
if($_POST) {
	$handler = post_handler($_POST);
	//var_dump($_POST);
	//var_dump($handler);
	$condition = get_conditons($_POST);
	$films_id=$sqlwrap->find_films($condition);
	//var_dump($films_id);
	//echo films($films_id);
}
echo '<form method="POST" name="1" action="Order.php">';
foreach ($attributes as $attribute){
	drop_down($attribute,$sqlwrap);
	if(isset($handler[$attribute])) {
		$count = count($handler[$attribute]);
		$i = 0;
		foreach ($handler[$attribute] as $old_val=>$x) {
			$old_value = explode ("|",
				$old_val);
			check_box($attribute.",".$i,
				$old_val);
			echo $old_value[1];
			$i++;
		}
		echo "<br>";
	}
}
echo '<input type="submit" value="Submit">
</form>';
echo '<form method="POST" name="1" action="OrderProcess.php">';
echo '<input list = "films" name = "film">'; 
echo '<datalist id= "films">';
$rule = "";
if ($films_id) {
	$rule = film_rule($films_id);
}
$films = films($sqlwrap ,$rule);
foreach ($films as $film) {
	$filmid = implode (",",$film);
	echo '<option value="'.$filmid.'">';
}
echo '</datalist>';
echo '<input type="submit" value="Rent">
</form>';
