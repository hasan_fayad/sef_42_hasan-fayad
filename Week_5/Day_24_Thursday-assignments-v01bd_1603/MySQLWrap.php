<?php

class MySQLWrap {
  
  private $dbCon ;
  private $query ;
  private $map = array(
    'actor' => 'film_actor',
    'category' => 'film_category',
    'language' => 'film',
    );
  public $film_array = array();
  
  function connect($host, $user, $pass, $db) {
    $this->dbCon = new mysqli($host, $user, $pass, $db);
    if ($this->dbCon->connect_error) {
      return 0;
    } else {
      return 1;
    }
  }
  
  function join_query($array) {
    $query = "SELECT film.film_id ,film.title ,film.description FROM film";
    foreach ($array as $key=>$element) {
      if ($element) {
        $query .= " INNER JOIN ".$key;
        $query .= " ON".$key."_";
      }
    }
  }
  
  function select_query($table ,$columns) {
    $query = "SELECT ";
    foreach ($columns as $key=>$column) {
      if($key == count($columns)-1) {
        $query .= $column;
      } else {
        $query .= $column." ,";
      }
    }
    $query .= " FROM ".$table;
    return $query;
  }
  
  function find_films($conditions) {
    $columns = array("film_id","");
    foreach ($conditions as $name => $condition) {
      $columns[1] = $name."_id";
      $table = $this->map[$name];
      $table .= " order by ".$name."_id";
      $query = $this->select_query($table,$columns);
      $result = $this->exec_query($query);
      //print_r($result);
      foreach($condition as $cond) {
        $this->search($result ,$cond ,$name."_id");
      }
    }
    return $this->film_array; 
  }

  function search($result ,$condition ,$col) {
    $start = 0;
    $count = count($result)-1;
    $end = $count;
    while ($start <= $end) {
      $index = $start + $end ;
      $index -= $index%2;
      $index /= 2;
      if ($result[$index][$col] == $condition) {
        //print_r($result[$index]);
        break;
      } elseif ($result[$index][$col] < $condition) {
        $start = $index + 1;
      } else {
        $end = $index - 1;
      }
    } 
    for ($i = $index ;$i < $count ;$i++) {
      $film_id = $result[$i]["film_id"];
      if ($result[$i][$col] == $condition) {
        if(isset($this->film_array[$film_id])) {
          $this->film_array[$film_id] +=1; 
        } else {
          $this->film_array[$film_id] =1;
        }
      } else {
        break;
      }
    }
    for ($i = $index-1 ;$i > 0 ;$i--) {
      $film_id = $result[$i]["film_id"];
      if ($result[$i][$col] == $condition) {
        if(isset($this->film_array[$film_id])) {
          $this->film_array[$film_id] +=1; 
        } else {
          $this->film_array[$film_id] =1;
        }
      } else {
        break;
      }
    }
  }

  function exec_query($query) {
    $row_array = array(array());
    $result = $this->dbCon->query($query);
    if(!$result) {
      return "error";
    } else {
      $rows = $result->num_rows;
      for ($i = 0; $i < $rows; $i++) {
        $result->data_seek($i);
        $row_array[$i] = $result->fetch_array(MYSQLI_ASSOC);
      }
      $result->close();
      return $row_array;
    }
  }
  
}

//$o = new MySQLWrap();
//$o->connect();
//$o->find_films(array('category'=>array("1")));
//var_dump($o->film_array);