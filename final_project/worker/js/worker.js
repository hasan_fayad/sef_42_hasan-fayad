function obj() {
	this.colorArray = [];
};

obj.prototype.init = function(oMAin) {
	for (var key in oMAin) {
    	//copy all the fields
       	this[key] = oMAin[key];
    }
}

obj.prototype.test = function(i, j) {
	x = this.startx + i * this.stepx;
	y = this.starty - j * this.stepy;
	real = 0;
	img = 0;
	for(k = 0; k< this.iteration && (real * real + img * img) < 4; k++) {
		for(p = 1; p < this.power; p++)	{
			tempReal = real;
			real = real * real - img * img;
			img =  tempReal * img * 2 ;
		}
		real += x;
		img += y;
	}
	return k * (256 / this.iteration);
} 

obj.prototype.jul = function(i, j){
	real = this.startx + i * this.stepx;
	img = this.starty - j * this.stepy;
	x = this.X;
	y = this.Y;
	for(k = 2; k< this.iteration + 2 && (real * real + img * img) < 4; k++) {
		for(p = 1; p < this.power; p++)	{
			tempReal = real;
			real = real * real - img * img;
			img =  tempReal * img * 2;
		}
		real += x;
		img += y;
	}
	return k * (256 / this.iteration);
}

obj.prototype.compress = function() {
	len = this.colorArray.length;
	color = 0;
	index = 0;
	counter = 0;
	smallArray = [];
	for(i = 0; i< len; i++) {
		if(this.colorArray[i] == color) {
			counter ++;
			if(counter > 1) {
				smallArray[index -1] = - counter
			} else {
				smallArray[index] = color ;
				index++;
			}
		} else {
			counter = 0;
			color = this.colorArray[i];
			smallArray[index] = color ;
			index ++;
		}
	}
	this.colorArray = smallArray;
}				

obj.prototype.explore = function(i0, i2, j0, j2, c00, c02 ,c20 ,c22) {
	if(j0 == j2 - 1) {
		this.exact(i0, i2, j0, j2);
		return 1;
		this.setColor(i0, j0, c00);
		this.setColor(i2, j0, c20);
		this.setColor(i0, j2, c02);
		this.setColor(i2, j2, c22);
		if(i0 == i2 - 1) {
			return 1 ;
		} else {
			this.exact(i0 + 1, i2, j0, j2);
		}
		return 1;
	} else if(c00==c02&&c00==c20&&c00==c22) {
		this.fill(i0, i2, j0, j2, c00);
		return 1;
	} else {
		var i1 = Math.floor((i2 + i0) / 2);
		var j1 = Math.floor((j2 + j0) / 2);	
		var c01 = this.test(i0, j1);
		var c10 = this.test(i1, j0);
		var c11 = this.test(i1, j1);
		var c12 = this.test(i1, j2);
		var c21 = this.test(i2, j1);
		this.explore(i0, i1, j1, j2, c01, c02, c11, c12);
		this.explore(i1, i2, j0, j1, c10, c11, c20, c21);
		this.explore(i0, i1, j0, j1, c00, c01, c10, c11);
		this.explore(i1, i2, j1, j2, c11, c12, c21, c22);
		return 1;	
	}
}

obj.prototype.fill = function(i0, i2, j0, j2, color) {
	for(var j = j0; j< j2; j++) {
		index = (j-this.y) * (this.splitSizeX) + i0 -this.x;
		for(var i = i0; i< i2; i++) {
		    this.colorArray[index] = color;
		    index ++;
		}
	}
}

obj.prototype.setColor = function(i, j, color) {
	index = (j - this.y) * this.splitSizeX + i -this.x;
	this.colorArray[index] = color;
}

obj.prototype.exact = function(i0, i2, j0, j2) {
	for(var j = j0; j< j2; j++) {
		index = (j-this.y) * (this.splitSizeX) + i0 -this.x;
		for(var i = i0; i< i2; i++) {
		    this.colorArray[index] = this.test(i, j);
		    index ++;
		}
	}
}

obj.prototype.mem = function(i0, i2, j0, j2) {
	mem = [];
		for (i = i0; i < i2; i++) {
		  	mem[i] = -1;
		}
	counter = 0;
	colo = -1;
	for(var j = j0; j < j2; j++) {
		index = (j - this.y) * (this.splitSizeX) + i0 - this.x;
		for(var i = i0; i < i2; i++) {
			if(mem[i] != -1) {
				this.colorArray[index] = mem[i];
				counter --;
			} else {
				this.colorArray[index] = o.test(i, j);
			}
			if(colo == this.colorArray[index]){
	    		counter++;
		    } else {
		        colo = this.colorArray[index] ;
		        counter = 0;
		    }
		    if(counter>1){
		        mem[i]=colo;
		    } else {
		        mem[i]=-1;
		    }
			index++;
		}
	}
}
onmessage = function(e) {
	o = new obj();
	oMAin = JSON.parse(e.data);
	o.init(oMAin);
	if(o.julia){
		o.test = o.jul;
	}
	while(o.cood.length > 0){
		o.x = o.cood[0].x;
		o.y = o.cood[0].y;
		o.cood = o.cood.slice(1);
		i0 = o.x;
		i2 = o.x + o.splitSizeX;
		j0 = o.y;
		j2 = o.y + o.splitSizeY;
		c00 = o.test(i0, j0);
		c02 = o.test(i0, j2);
		c20 = o.test(i2, j0);
		c22 = o.test(i2, j2);
		if(o.alg == 0) {	
			o.exact(i0, i2, j0, j2);
		} else if(o.alg == 1) {
			o.explore(i0, i2, j0, j2, c00, c02, c20, c22);
		} else if(o.alg == 2){
			o.mem(i0, i2, j0, j2);
		}
		o.compress();
		postMessage(JSON.stringify(o));
	}
}
