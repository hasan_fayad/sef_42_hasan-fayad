inst = new instObject();
Anim=0;
canvas = new canvasObject(inst);
threadsinst = new threadsInst(inst);
threadsinst.all();
var worker = []; 
f = 1;
//setInput();
function toggle(){
	if(inst.julia){
		document.getElementById("julia").parentNode.setAttribute("class", "toggle btn btn-primary");
	} else {
		document.getElementById("julia").parentNode.setAttribute("class", "toggle btn btn-default off");
	}	
	if(inst.color){
		document.getElementById("color").parentNode.setAttribute("class", "toggle btn btn-primary");
	} else {
		document.getElementById("color").parentNode.setAttribute("class", "toggle btn btn-default off");
	}	
}

callThreads(threadsinst, canvas, worker);
document.addEventListener('keydown', function(event) {
    if(!inst.interactive && !isWorking(worker, inst)) {
	    terminateAll(worker, inst);
	    xtrans = 0;
	    ytrans = 0;
	    xOff = Math.ceil(inst.width / inst.threadCount);
	    yOff = Math.ceil(inst.height / inst.threadCount);
	    if(event.keyCode == 40) {
	     	ytrans = - inst.transOff;
	    }
	    else if(event.keyCode == 38) {
	     	ytrans = inst.transOff;
	    }
	    else if(event.keyCode == 39) {
	     	xtrans = - inst.transOff;
	    }
	    else if(event.keyCode == 37) {
	     	xtrans = inst.transOff;
	    } else {
	    	return 1;
	    }
	    inst.startx -= inst.stepx * xtrans; 
	    inst.starty += inst.stepy * ytrans;
	    canvas.context.putImageData(canvas.imagedata, xtrans, ytrans);
	    canvas.imagedata = canvas.context.getImageData(0,0,inst.width,inst.height);
	    threadsinst = new threadsInst(inst);
	    threadsinst.translate(xtrans, ytrans, xOff, yOff);
	    callThreads(threadsinst, canvas, worker);
	}
}, false);


document.getElementById("render").addEventListener("click", function(){
	getInput();
	terminateAll(worker, inst);
	execute(inst);
});

document.getElementById("fractal").addEventListener("click", function(){
	f=1;
	clearInterval(Anim);
	document.getElementById('setting').style.display = "block";
	terminateAll(worker, inst);
	collor = inst.color;
	interactiv = inst.interactive;
	jul = inst.julia;
	inst = new instObject();
	inst.color = collor;		
	inst.interactive = interactiv;
	inst.julia = jul;
	canvas = new canvasObject(inst);
	setInput();
	worker = [];
	execute(inst);
	if(interactiv) {
		call();
		inst.interactive = 1;
	}
	
});

document.getElementById("snow").addEventListener("click", function(){
	f=0;
	terminateAll(worker, inst);
	clearInterval(Anim);
	interactive = 0;
	document.getElementById('setting').style.display = "none";
	snow();
});

document.getElementById("power").addEventListener("click", function(){
	f=0;
	terminateAll(worker, inst);
	clearInterval(Anim);
	interactive = 0;
	document.getElementById('setting').style.display = "none";
	power(0.05,0,0.05);
});
//document.getElementById("interactive").addEventListener("click", function(){
function interact(){	
	inst.interactive = ! inst.interactive;
	if(inst.interactive) {
		//document.getElementById("interactive").innerHTML = "normal";
		//instFire = firebase.database().ref('instruction');
		call();
	} else {
		//document.getElementById("interactive").innerHTML = "interactive";
		instFire.off();
	}
}

function call(){
	var instFire = firebase.database().ref('instruction');
		instFire.on('value', function(snapshot) {
			if(inst.interactive) {
				//console.log(snapshot.val())
				terminateAll(worker, inst);
				inst = snapshot.val();
				setInput();
				canvas = new canvasObject(inst);
				threadsinst = new threadsInst(inst);
			    threadsinst.all();
			    callThreads(threadsinst, canvas, worker);
			}
		});
}

function getInput() {
	inst.iteration = document.getElementById("iteration").value*1;
	inst.zoomLevel = document.getElementById("zoomLevel").value*1;
	inst.threadCount = document.getElementById("threadCount").value*1;
	inst.splitNum = document.getElementById("splitNum").value*1;
	inst.alg = document.getElementById("alg").value*1;
	inst.transOff = document.getElementById("transOff").value*1;
	inst.julia = document.getElementById("julia").checked;
	inst.color = document.getElementById("color").checked;
	inst.power = document.getElementById("pow").value*1;
	inst.X = document.getElementById("X").value*1;
	inst.Y = document.getElementById("Y").value*1;
	//inst.interactive = document.getElementById("interactive").checked;
}

function setInput() {
	document.getElementById("iteration").value = inst.iteration;
	document.getElementById("zoomLevel").value = inst.zoomLevel;
	document.getElementById("threadCount").value = inst.threadCount;
	document.getElementById("splitNum").value = inst.splitNum;
	document.getElementById("alg").value = inst.alg;
	document.getElementById("transOff").value = inst.transOff;
	document.getElementById("julia").checked = inst.julia;
	toggle();
	document.getElementById("color").checked = inst.color;
	document.getElementById("power").value = inst.power;
	document.getElementById("X").value = inst.X;
	document.getElementById("Y").value = inst.Y;
	//document.getElementById("interactive").checked = inst.interactive;
}

canvas.canvas.addEventListener("mousedown", function(event) {
	if(f){	   
		XX = event.clientX - canvas.canvas.getBoundingClientRect().left;
		YY = event.clientY - canvas.canvas.getBoundingClientRect().top;
		console.log(XX)
	    getInput();
	    terminateAll(worker, inst);
	    if (event.which === 3) { // right click = 3, left click = 1
	    inst.startx += (XX - inst.width / (2 / inst.zoomLevel)) * inst.stepx ;
	    inst.starty -= (YY - inst.height / (2 / inst.zoomLevel)) * inst.stepy ;
	    inst.stepx *= inst.zoomLevel;
	    inst.stepy *= inst.zoomLevel;
	    inst.iteration -= 32 * (inst.zoomLevel - 1);
	    }
	    if (event.which === 1) { // right click = 3, left click = 1
	    inst.startx += (XX - inst.width / (inst.zoomLevel * 2)) * inst.stepx ;
	    inst.starty -= (YY - inst.height / (inst.zoomLevel * 2)) * inst.stepy ;
	    inst.stepx /= inst.zoomLevel;
	    inst.stepy /= inst.zoomLevel;
	    inst.iteration += 32 * (inst.zoomLevel - 1);
	    }
	 	execute(inst);
	} else {
		clearInterval(Anim);
	}
});

function execute(inst){
	if(!inst.interactive) {
    	//canvas = new canvasObject(inst);
    	threadsinst = new threadsInst(inst);
    	threadsinst.all();
    	callThreads(threadsinst, canvas, worker);
    	document.getElementById("iteration").value = inst.iteration;
	} else {
		firebase.database().ref('instruction').set(inst);
	}
}
// prevent context menu show up
document.addEventListener('contextmenu', function(e) {
    e.preventDefault();
}, false);


function downloadCanvas(link, canvasId, filename) {
    link.href = document.getElementById("canvas").toDataURL();
    link.download = filename;
}

document.getElementById('download').addEventListener('click', function() {
    downloadCanvas(this, 'canvas', 'test.png');
}, false);