 var blob = new Blob([
    	document.getElementById("worker").textContent
    ], { type: "text/javascript" });

function instObject(){
	this.splitNum = 16;
	this.width = Math.floor(window.innerWidth*0.8) - Math.floor(window.innerWidth*0.8) % this.splitNum;
	this.height = window.innerHeight - window.innerHeight % this.splitNum;
	this.startx = -4;
	this.starty = 2;
	this.stepx = 8 / this.width;
	this.stepy = 4 / this.height;
	this.threadCount = 8;
	this.iteration = 64;
	this.alg = 0;
	this.zoomLevel = 4;
	this.transOff = 150;
	this.julia = 0;
	this.power = 2;
	this.X = -1;
	this.Y = 0.3;
	this.interactive = 0;
	this.color = 0;
}

function canvasObject(instObject) {
	w = Math.floor(window.innerWidth*0.8) - Math.floor(window.innerWidth*0.8) % instObject.splitNum;
	h = window.innerHeight - window.innerHeight % instObject.splitNum;
	this.canvas = document.getElementById("canvas");
	this.canvas.width = instObject.width;
	this.canvas.height = instObject.height;
	this.context = this.canvas.getContext("2d");
	this.imagedata = this.context.createImageData(this.canvas.width, this.canvas.height);
	this.canvas.style.top = (h - instObject.height) / 2 + "px";
	this.canvas.style.left = (w - instObject.width) / 2 + "px";
	//console.log((instObject.height - h) / 2 + "px")
}

function threadsInst(instObject) {
	this.thread = [{}];
	for(var i = 0; i < instObject.threadCount; i++) {
		this.thread[i] = {
			cood : [{}],
		}
		for (var key in instObject) {
        //copy all the fields
        	this.thread[i][key] = instObject[key];
    	}
	}
}

threadsInst.prototype.all = function() {
	sObject = spiralArray(this.thread[0]);
	for(var i = 0; i < this.thread[0].threadCount; i++) {
		this.thread[i].splitSizeX = sObject.splitSizeX;
		this.thread[i].splitSizeY = sObject.splitSizeY;
	}
	counter = this.thread[0].splitNum * this.thread[0].splitNum;
	for(var i = 0; i < counter; i++) {
		this.thread[i % this.thread[0].threadCount].cood.push(sObject.sArray[i]); 
	}
}

threadsInst.prototype.translate = function(xx, yy, xOff, yOff) {
	tArray = []; 
	width = this.thread[0].width;
	height = this.thread[0].height; 
	if(xx < 0)
	for(i = 0; i < this.thread[0].threadCount; i++) {
		tArray[i] = [{
			x : width + xx,//- xOff - width % xOff,
			y : i * yOff,
		}]
		xOff = - xx;
	}
	if(xx > 0)
	for(i = 0; i < this.thread[0].threadCount; i++) {
		tArray[i] = [{
			x : 0,
			y : i * yOff,
		}]
		xOff = xx;
	}
	if(yy > 0)
	for(i = 0; i < this.thread[0].threadCount; i++) {
		tArray[i] = [{
			x : i * xOff,
			y : 0,
		}]
		yOff = yy;
	}
	if(yy < 0)
	for(i = 0; i < this.thread[0].threadCount; i++) {
		tArray[i] = [{
			x : i * xOff,
			y : height + yy,//- yOff - height % yOff,
		}]
		yOff = - yy;
	} 
	for(i = 0; i < this.thread[0].threadCount; i++) {
		this.thread[i].splitSizeX = xOff;
		this.thread[i].splitSizeY = yOff;
		this.thread[i].cood = tArray[i];
	}
	//console.log(this)
}

function spiralArray(ins) {
	width = ins.width;
	height = ins.height;
	splitNum = ins.splitNum;
	splitSizeX = (width - width % splitNum) / splitNum; 
	splitSizeY = (height - height % splitNum) / splitNum;
	sArray = [];
	firstX = splitSizeX * ((splitNum - splitNum % 2) / 2 + splitNum % 2 - 1);
	firstY = splitSizeY * ((splitNum - splitNum % 2) / 2 - 1 + splitNum % 2);
	sArray[0] = {
		x : firstX,
		y : firstY,
	};
	count = 1;
	sign = 1;
	while(count < splitNum ){	
		for(i = 0; i < 2; i++) {
			for(j = count * (count - 1); j < count * (count - 1) + count; j++) {
				sArray[j + count * i + 1] = {
					x : sArray[j + count * i ].x 
						+ (1 - i % 2) * (splitSizeX * sign),
					y : sArray[j + count * i ].y 
						+ (i % 2) * (splitSizeY * sign),	
				};
			}
		}
		count++;
		sign *= -1;
	} 
	for(j = count * (count - 1); j < count * (count - 1) + count - 1; j++) {
		sArray[j + 1] = {
			x : sArray[j ].x + splitSizeX * sign, 
			y : sArray[j ].y,	
		};
	}
	o = {
		sArray : sArray,
		splitSizeX : splitSizeX,
		splitSizeY : splitSizeY,
	}
	return o;
}

function decompress(array) {
	length = array.length;
	index = 0;
	bigarray = [];
	for(i = 0; i < length; i++) {
		if(array[i] >= 0) {
			bigarray[index] = array[i];
			index++;
		} else {
			for(j = 0; j < - array[i] ; j++) {
				bigarray[index] = array[i - 1];
				index++;
			}
		}
	}
	return bigarray; 
}

function callThreads(object, canvas, worker) {
	for(i = 0; i < inst.threadCount; i++){
		start = new Date().getTime();
		o = object.thread[i];
		o.id = i;
	    worker[i] = new Worker(window.URL.createObjectURL(blob));
	    worker[i].postMessage(JSON.stringify(o));
	    worker[i].onmessage = function(e) {
		  	window.setTimeout(function(){
		  		o = JSON.parse(e.data);
		  		o.colorArray = decompress(o.colorArray);
		  		canvas.render(o);
		  		delete o.colorArray;
		  		if(o.cood.length == 0){
		  			worker[o.id].terminate();
		  			worker[o.id] = 0;
		  		}
	  		}, 0);
		} 
	}
}

canvasObject.prototype.render = function(o, worker){
  	index1 = 0;
  	for (var j = o.y; j < o.y + o.splitSizeY; j++) {
	  	index = ((j) * width + o.x)*4;
	  	for (var i=o.x; i<o.x+o.splitSizeX; i++, index1++) {
		    colo = (o.colorArray[index1]);
			if(o.color){
			    this.imagedata.data[index++] = colo%64;
			    this.imagedata.data[index++] = colo%32;
		    	this.imagedata.data[index++] = colo%16;
			    this.imagedata.data[index++] = 255 - colo;
			} else {
				this.imagedata.data[index++] =colo;
			    this.imagedata.data[index++] =colo;
		    	this.imagedata.data[index++] =colo;
			    this.imagedata.data[index++] = 255-colo;
			}
		}
	}
  	this.context.putImageData(this.imagedata, 0, 0);
}

function terminateAll(worker, inst){
	for(i = 0; i < worker.length; i++){
		if(worker[i]!=0){
		worker[i].terminate();
		}
	}
	worker = [];
}

function isWorking(worker, inst){
	for(i = 0; i < worker.length; i++){
		if(worker[i]!=0){
			return 1;
		}
	}
	return 0;
}

