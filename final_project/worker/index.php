<?php 
    $worker = file_get_contents("js/worker.js");
?>
<html class="no-js" lang="">
    <head>
        <script src="https://www.gstatic.com/firebasejs/3.5.0/firebase-app.js"></script>
        <script src="https://www.gstatic.com/firebasejs/3.5.0/firebase-auth.js"></script>
        <script src="https://www.gstatic.com/firebasejs/3.5.0/firebase-database.js"></script>
        <script src="https://www.gstatic.com/firebasejs/3.5.0/firebase-messaging.js"></script>
        <script>
            // Initialize Firebase
            var config = {
                apiKey: "AIzaSyCgihu5gEfQxuu5IIG7bozil7oMDvCC7Ms",
                authDomain: "fractal-bb691.firebaseapp.com",
                databaseURL: "https://fractal-bb691.firebaseio.com",
                storageBucket: "",
                messagingSenderId: "173964629476"
            };
            firebase.initializeApp(config);
            var database = firebase.database();
            //const messaging = firebase.messaging();            
        </script>
        <script src="http://code.jquery.com/jquery-latest.min.js"
        type="text/javascript"></script>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
            <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
            <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/main.css">
    </head>
    <body >
        <div class="pull-left col-md-10" style="padding:0;position:absolute">
        <canvas id="canvas" ></canvas>
        </div>
        <div id="setting" class="form-group pull-right col-md-2" style="height:100vh;background-color:white;overflow:scroll;padding:10px 10px; margin:0;">
            <div class="form-group">
                <label for="interactive" >interactive</label>
                <input type="checkbox" id ="interactive" onclick="interact()" onchange="interact()"  data-toggle="toggle">
            </div>
            <div class="form-group">
                <label for="color" >color</label>
                <input type="checkbox" id ="color" data-toggle="toggle">
            </div>    
                <div class="form-group">
                <label for="iteration">iteration</label>
                <input type="number" id ="iteration" value = "64"  class="form-control">
            </div>    
            <div class="form-group">
                <label for="zoomLevel">zoomLevel</label>
                <input type="number" id ="zoomLevel" value = "4"  class="form-control">
            </div> 
            <div class="form-group">
                <label for="threadCount">threadCount</label>
                <input type="number" id ="threadCount" value = "8"  class="form-control">
            </div> 
            <div class="form-group">
                <label for="splitNum">splitNum</label>
                <input type="number" id ="splitNum" value = "16"  class="form-control">
            </div>
            <div class="form-group">
                <label for="alg">alg</label>
                <input type="number" id ="alg" value = "0"  class="form-control">
            </div>
            <div class="form-group">
                <label for="pow">power</label>
                <input type="number" id ="pow" value = "2"  class="form-control">
            </div>  
            <div class="form-group">
                <label for="transOff">transOff</label>
                <input type="number" id ="transOff" value = "150"  class="form-control">
            </div> 
            <div class="form-group">
                <label for="julia">julia</label>
                <input type="checkbox" id ="julia"   data-toggle="toggle" >
            </div> 
            <div class="form-group">
                <label for="X">X</label>
                <input type="number" id ="X" value = "-1"  class="form-control">
            </div> 
            <div class="form-group">
                <label for="Y">Y</label>
                <input type="number" id ="Y" value = "0.3"  class="form-control">
            </div> 
            <button type="button" id="render"  class="btn btn-default" data-dismiss="modal">render</button>
              </div>
        </div>
        <div id="controller" >
  <!--        <div class="form-group">
         <a href="#" class="btn btn-success btn-lg" id="reset"><span class="glyphicon glyphicon-refresh" ></span></a>
         </div> -->
        <div class="form-group">
        <a href="#" class="btn btn-success btn-lg" id="fractal"><b>F</b></a>
        </div>
        <div class="form-group">
        <a href="#" class="btn btn-success btn-lg" id="power"><b>P</b></a>
         </div>
          <div class="form-group">
          <a href="#" class="btn btn-success btn-lg" id="snow"><b>S</b></a>  </div> 
          <div class="form-group">
          <a href="#" class="btn btn-success btn-lg" id="download"><span class="glyphicon glyphicon-download-alt" ></span></a>  
          </div>
          <div class="form-group">
            <button type="button" class="btn btn-default"><a href="https://hasan_fayad@bitbucket.org/hasan_fayad/sef_42_hasan-fayad.git">Bitbucket</a></button>
          </div>
             <!-- <div class="form-group">
            <button type="button" id="reset"  class="btn btn-primary  ">reset</button>
            </div> -->
        </div>
        <!-- Trigger the modal with a button -->


       
        <script id="worker">
            <?php
                echo $worker;
            ?>
        </script>
        <script src="js/view.js?n=1"></script>
        <script src="js/controller.js?n=1"></script>
       
        <script src="js/snow.js?n=1"></script>
        <script src="js/power.js?n=1"></script>
    </body>
</html>
