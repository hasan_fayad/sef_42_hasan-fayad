<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="css/main.css">

        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <div id="main">    
            <div>    
                <form action="javascript:;">
                    <div>
                        <input type="text" id="url" 
                            value="https://medium.com/the-dot-and-line/cowboy-bebop-personal-essay-b2d4881f65bc#.kmyuv0jtl"/>
                    </div>
                    <div>    
                        <button id="url_grabber" class="item-add-button">Grab</button>
                    </div>
                </form>
            </div>
            <div id="loading"></div>
            <div>
                <form id="text_form">
                    <div id="title_div"></div>
                    <div id="text_div"></div>
                    <div id="button_div"></div>
                </form>
            </div>
            <div id="summary"><p id="sum_p"></p></div>
        </div>
        <script src="js/main.js"></script>
    </body>
</html>
