Object.prototype.set = function(title, text) {
  this.title = title;
  this.text = text;  
};

Object.prototype.getSum = function() {
  ajax_post(this, 2, this.display_sum);
}

Object.prototype.display_sum = function(sum) {
  document.getElementById("sum_p").innerHTML = sum;
}
var Object = function(url) {
  this.url = url;  
};

Object.prototype.getUrlContent = function() {
  ajax_post(this, 1, this.extract_text);
}

Object.prototype.extract_text = function(html_str) {
  var SCRIPT_REGEX = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi;
  while (SCRIPT_REGEX.test(html_str)) {
    html_str = html_str.replace(SCRIPT_REGEX, "");
  }
  temp = document.createElement("div");
  temp.innerHTML = html_str;
  titl = temp.getElementsByClassName('graf--title');
  this.title = titl[0].textContent || titl[0].innerText;
  //this.title = titl[0].innerHTML;
  par=temp.getElementsByClassName("graf--p");
  var orgText = "";
  for(var i = 0; i < par.length; i++) {
    if((par[i].innerHTML).length > 100){
      orgText = orgText + par[i].innerHTML;
    }
  }
  temp.innerHTML = orgText;
  this.text = temp.textContent || temp.innerText;
  temp.remove();
  display_text(this);
} 

function display_text(object) {
  var title = document.createElement("INPUT");
  title.setAttribute("type", "text");
  title.value = object.title;
  title.id = "title";
  var text = document.createElement("TEXTAREA");
  text.id = "text";
  text.value = object.text;
  var sum = document.createElement("button");
  sum.id = "sum";
  sum.innerHTML = "Sum";
  sum.setAttribute('type', 'button');
  sum.addEventListener('click', function() {summarize(object);}, false);
  document.getElementById("title_div").appendChild(title);
  document.getElementById("text_div").appendChild(text);
  document.getElementById("button_div").appendChild(sum);
}

function ajax_post(object, post_id, func) {
  loading.Start();
  post_notation = object_to_post_notation(object, post_id);
  // Create our XMLHttpRequest object
  var xhr = new XMLHttpRequest();
  // Create some variables we need to send to our PHP file
  var url = "tst.php";    
  xhr.open("POST", url, true);
  xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  // Access the onreadystatechange event for the XMLHttpRequest object
  xhr.onreadystatechange = function() {
      if(xhr.readyState == 4 && xhr.status == 200) {
          loading.Stop();
          var returned_data = xhr.responseText;alert(returned_data);
          func(returned_data);
          //document.write(extract_p(returned_data));
      }
  }
  // Send the data to PHP now... and wait for response to update the status div
  xhr.send(post_notation); // Actually execute the request
}

function object_to_post_notation(object, post_id) {
  var post_notation = "id=" + post_id;
  for (elts in object) {
    str = object[elts] + "";
    if(str.indexOf("function") == -1) {
      post_notation += "&" + elts + "=";
      post_notation += object[elts];
    }
  }
  return post_notation;
}

//object that does loading animation
var loading = {
    slider: null,
    Start: function() {
        this.slider = setInterval(function() {
          message = ".....loading.....";
          cur_msg = document.getElementById("loading").innerHTML;
          if(cur_msg.length == message.length) {
            cur_msg = "";
          }
          new_msg = message.substr(0, cur_msg.length+1);
          document.getElementById("loading").innerHTML = new_msg;
        }, 500);
    },
    Stop: function() {
      document.getElementById("loading").innerHTML = "";
      window.clearTimeout(this.slider);
    }
};

function ValidURL(url) {
  var regex = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
  if(!regex .test(url)) {
    alert("Please enter valid URL.");
    return false;
  } else {
    return true;
  }
}


function postStuff(vars){
// Create our XMLHttpRequest object
var hr = new XMLHttpRequest();
// Create some variables we need to send to our PHP file
var url = "tst.php";
hr.open("POST", url, true);
hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
// Access the onreadystatechange event for the XMLHttpRequest object
hr.onreadystatechange = function() {
    if(hr.readyState == 4 && hr.status == 200) {
        var return_data = hr.responseText;
    }
}
// Send the data to PHP now... and wait for response to update the status div
hr.send(vars); // Actually execute the request
}

document.getElementById("url_grabber").addEventListener("click", function(){
  url = document.getElementById("url").value;
  if (ValidURL(url)) { 
    document.getElementById("title_div").innerHTML = "";
    document.getElementById("text_div").innerHTML = "";
    document.getElementById("button_div").innerHTML = "";
    document.getElementById("sum_p").innerHTML = "";
    object = new Object(url);
    object.getUrlContent();
  }  
});

function summarize(object){
  title = document.getElementById("title").value;
  text = document.getElementById('text').value;
  document.getElementById("sum_p").innerHTML = "";
  object.set(title, text);
  object.getSum();
}