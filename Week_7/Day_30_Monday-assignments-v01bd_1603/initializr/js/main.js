/**
 * this function take data from all the form fields
 * and put them in an object for_obj ,
 * adds the date time to this object and append it to
 * the local storage and display its content on the web app
 * */
function form_to_obj() {
  var form_obj = {};
  var elements = document.getElementById("form").elements;
  for(var i = 0 ; i < elements.length ; i++){
    var item = elements.item(i);
    if(item.name != "add" ) {
      if(item.name == "task" && item.value === "") {
        alert("main field can 't be empty");
        return 0;
      }
      if(item.value.indexOf('[next]') != -1||
          item.value.indexOf('[key_value]') != -1) {
        alert("unsupported values");
        return 0;
      }
      form_obj[item.name] = item.value;
    }
  }
  date = Date();
  date = date.substring(0, date.indexOf('GMT'));
  form_obj.time = date;
  key = get_key();
  appendToStorage(key, form_obj);
  document.getElementById("form").reset();
  display(key, form_obj);
}

//takes an object and key ,stringuify the oject and put it in the local storage
function appendToStorage(key, form_obj){
  var value = "";
  for (var name in form_obj) {
    value += name + "[key_value]";
    value += form_obj[name] + "[next]";
  }
  //remove last [next] from the string
  value = value.substring(0, value.length - 6);
  localStorage.setItem(key, value);
}

//return the key at which data should be put
function get_key() {
  //key of last input
  last_key = localStorage.key(localStorage.length - 1);
  if(last_key === null) {
    key = 1 ;
  } else {
    last_key = parseInt(last_key);
    key = last_key + 1;
  }
  //to order number according to string ordering rules
  key = String("00000" + key).slice(-5);
  return key;
}

//turn a string into an object
function string_to_object(str) {
  var obj = [];
  //next is what separate two object elements
  obj_array = str.split("[next]");
  for(i = 0; i < obj_array.length; i++) {
    //key_value seperate key and value of an element
    key_value = obj_array[i].split("[key_value]");
    key = key_value[0];
    value = key_value[1];
    obj[key] = value;
  }
  return obj;
}

function display(id, object) {
  var task = document.createElement('div');
  task.id = id;
  task.className = "tasks";
  referenceNode = document.getElementById("form-container");
  referenceNode.parentNode.insertBefore(task, referenceNode.nextSibling);
  var div = document.createElement('div');
  task.appendChild(div);
  var h2 = document.createElement('h2');
  div.appendChild(h2);
  var p = document.createElement('p');
  div.appendChild(p);
  var h3 = document.createElement('h3');
  div.appendChild(h3);
  var del = document.createElement('button');
  task.appendChild(del);
  h2.innerHTML = object.task;
  p.innerHTML = "added: " + object.time;
  h3.innerHTML = object.task_desc;
  attr = "remove(\"" + id + "\")";
  del.setAttribute("onclick", attr);
  del.innerHTML = "x";
}

/**
 * get all data from localStorage and display them
 * in the same time it reorder this data in case of removal
 * of data
 **/
function get_from_storage() {
  var counter = 1;
  for (var key in localStorage) {
    task_string = localStorage.getItem(key);
    new_key = String("00000" + counter).slice(-5);
    counter++;
    if(key != new_key) {
      localStorage.setItem(new_key, task_string);
      localStorage.removeItem(key);
    }
    task_obj = string_to_object(task_string);
    display(new_key, task_obj);
  }
}

//remove from Storage and display
function remove(key) {
  localStorage.removeItem(key);
  var element = document.getElementById(key);
  element.parentNode.removeChild(element);
}

get_from_storage();
