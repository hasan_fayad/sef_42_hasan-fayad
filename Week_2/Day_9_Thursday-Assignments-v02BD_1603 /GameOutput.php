<?php

// base class with member properties and methods
class GameOutput
{
  //the offset given in game solver
  private $offset;
  //the postfix notation
  private $result;
  //the wanted value
  private $value;
  
  function __construct($result,$offset,$value)
  {
    $this->result=$result;
    $this->offset=$offset;
    $this->value=$value;
  }
  
  //find the first mathematical sign and echo its predefined key
  //"+"--->0
  //"-"--->1
  //"*"--->2
  //"/"--->3
  function first_opp()
  {
    $symbol_val=array("+" => 0,"-" => 1,"*" => 2,"/" => 3);
    for($i=0;$i<count($this->result);$i++)
    {
      if(!is_numeric($this->result[$i]))
      {
        return $symbol_val[$this->result[$i]];
      }
    }
  }
  
  //return the infix notation
  function infix()
  {
    $stack=array();
    $bool=$this->first_opp();
    for($i=0;$i<count($this->result);$i++)
    {
      //push number into postfix stack
      if(is_numeric($this->result[$i]))
      {
        array_push($stack,$this->result[$i]);
      }
      else
      {
        //if last opp was not * add paretheses
        if($this->result[$i]=="*")
        {
          if($bool!=2)
          {
            $stack=$this->add_par($stack);
          }
          //reset the bool to multiplication key
          $bool=2;
          //evaluate the expression
          $stack=$this->expression($stack,$this->result[$i]);
        }
        elseif($this->result[$i]=="+")
        {
          if($bool!=0)
          {
            $stack=$this->add_par($stack);
          }
          $bool=0;
          $stack=$this->expression($stack,$this->result[$i]);
        }
        elseif($this->result[$i]=="-")
        {
          if($bool!=1)
          {
            $stack=$this->add_par($stack);
          }
          $bool=1;
          $stack=$this->expression($stack,$this->result[$i]);
        }
        else
        {
          if($bool!=3)
          {
            $stack=$this->add_par($stack);
          }
          $bool=3;
          $stack=$this->expression($stack,$this->result[$i]);
        }
      }
    }
    return $stack[0];
  }
 
  //add paretheses
  function add_par($stack)
  {
    $s=array_pop($stack);
    $s2=array_pop($stack);
    //if the value in the stack is a single number don't add a paretheses only concatinate
    //if it is an expression concatinate it with paretheses
    if(!is_int($s))
    {
      $s="(".$s.")";
    }
    if(!is_int($s2))
    {
      $s2="(".$s2.")";
    }
    array_push($stack,$s2);
    array_push($stack,$s);
    return $stack;
  }
  
  //concatinate last two value in a stack and pushes them to the stack
  function expression($stack,$sign)
  {
    $string=array_pop($stack);
    $string2=array_pop($stack);
    $string2.=$sign.$string;
    array_push($stack,$string2);
    return $stack;
  }
  
  //echo the desired output
  function output()
  {
    if($this->offset==0) $offset="EXACT";
    else $offset="Remaning: $this->offset";
    echo"\nSolution [".$offset."]:\n".$this->infix()." = ".($this->value+$this->offset)."\n";
  }
}
