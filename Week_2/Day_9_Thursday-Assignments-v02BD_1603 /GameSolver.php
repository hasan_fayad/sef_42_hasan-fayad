<?php

// base class with member properties and methods
class GameSolver
{
  private $flag=0;
  private $arr=array("+",'*',"-","/");
  private $value=0;
  private $result=array();
  private $offset=999;
  
  function __construct($num_array,$value)
  {
    $this->arr=array_merge($this->arr,$num_array);
    $this->value=$value;
  } 
  
  function solve(){
    $arr1=array();
    $stack=array();
    $this->recursion(0,$this->arr,array(),array(),$this->value);
  }
  //arr is numarray and arr1 is postfix and stack is offset calc stack
  function recursion($count,$array,$stack,$postfix,$val)
  {
    if(count($stack)==1&&abs($stack[0]-$val)<abs($this->offset))
    {
      $this->offset=$stack[0]-$val;
      $this->result=$postfix;
    }

    foreach ($array as $key => $value)
    {
      if(!$this->offset)
      {
        break;
      }
      if($key==7&&count($postfix)==0)
      {
        break;
      }
      $tempcount=$count;
      $temparray=$array;
      $tempstack=$stack;
      $temppostfix=$postfix;
      //find closer value

      if($key>3)
      {
        array_push($temppostfix,$value);
        array_push($tempstack,$value);
        unset($temparray[$key]);
        $this->recursion($count+1,$temparray,$tempstack,$temppostfix,$val);
      }
      elseif($count>=2)
      {
        
        if($this->is_valid_opp($value,$key,$temparray,$tempstack,$temppostfix))
        {
          $this->recursion($count-1,$temparray,$tempstack,$temppostfix,$val);
        }
        else
        {
          continue;
        }
      }
    }
  }

  function is_valid_opp($value,$key,&$temparray,&$tempstack,&$temppostfix)
  {
    $stack=$tempstack;
    $tail=array_pop($stack);
    $tail2=array_pop($stack);
    $calc=$this->calculate($key,$tail2,$tail);
    if($calc==0)
    {
      return 0 ;
    }
    else
    {
      array_pop($tempstack);
      array_pop($tempstack);
      array_push($tempstack,$calc);
    }
    array_push($temppostfix,$value );
    return 1;
  }
  
  function calculate($key,$num1,$num2)
  {
    if ($key==0)
    {
      return $num1+$num2;
    }
    if($key==1)
    {
      return $num1*$num2;
    }
    if($num1>$num2&&$num2)
    {
      if($key==2)
      {
        return $num1-$num2;
      }
      elseif($key==3&&$num1%$num2==0)
      {
        return $num1/$num2;
      }
    }
    return 0;
  }
  
  function get_result(){
    return $this->result;
  }
  
  function get_offset(){
    return $this->offset;
  }
}


