<?php
require_once('GameSolver.php');
require_once('GameGenerator.php');
require_once('GameOutput.php');

$max_time=0;
echo "How many games would you like me to play today?\n";
$number=trim(readline(),"\n");
echo is_int($number);

//only positif intiger are allowed
while(!(is_numeric($number) && $number > 0 && $number == round($number, 0)))
{
  $number=trim(readline(),"\n");
}

//loop according to input
for($i=0;$i<$number;$i++)
{
  echo "GAME: ".($i+1)."\n";
  $start_time=time();
  $generator=new GameGenerator();
  //the 6 digit sequence
  $rand1=$generator->generator();
  //the value you want to acheive
  $rand2=$generator->other_generator();
  $set="{ ";
  foreach ($rand1 as $elements)
  {
    $set.=$elements." ,";
  }
  echo $set." }";
  echo "\nTarget : ".$rand2;
  $solver=new GameSolver($rand1,$rand2);
  //the core function of the solver class
  $solver->solve();
  //wanted postfix notation
  $results=$solver->get_result();
  //by how many it missed the wanted value
  $offset=$solver->get_offset();
  $output=new GameOutput($results,$offset,$rand2);
  $output->output();
  $start_time-=time();
  $start_time=abs($start_time);
  if($start_time>$max_time)
  {
    $max_time=$start_time;
  }
  echo "Time : ".$start_time." s.\n\n---------------------------\n\n";
}
echo "MAX TIME = ".$max_time."\n";