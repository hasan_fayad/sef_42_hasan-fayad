<?php

// base class with member properties and methods
class GameGenerator
{
  private $num_array=array(25,50,75,100);
  private $num_array1=array(1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,10);
 
  
  function __construct()
  {
  }
  
  function generator()
  {
    $array=array();
    $temp_array=$this->num_array;
    shuffle($temp_array);
    $rand=rand(1,4);
    $array=array_merge($array,array_slice($temp_array,0,$rand));
    $temp_array1=$this->num_array1;
    shuffle($temp_array1);
    $array=array_merge($array,array_slice($temp_array1,0,6-$rand));
    return $array;
  }
  
  function other_generator()
  {
    return rand(101,999);
  }
}


