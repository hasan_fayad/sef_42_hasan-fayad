#!/usr/bin/php
<?php
/*
*function result transform the numerical value of the colors to the actual 
*color string and check if the guess is true or false and count the number of errors
*/
function result($color_array,$guess_array)
{
	$error=0;
	for($i=0;$i<count($color_array);$i++)
	{
		if($color_array[$i]==$guess_array[$i])
		{
			$check="true";
		}
		else
		{
			$check="false";
			$error++;
		}
		if($guess_array[$i]==1)
		{
			$guess="white";
		}
		else
		{
			$guess="black";
		}
		echo $guess."---->".$check."\n";
	}
	if($error<=1)
	{
		echo "you win\n";
	}
	else
	{
		echo "you loose\n";
	}
}
/*
*function makes_guess takes the index of the player the numerical color array
*and the guess array it multiply all the value inthe guess array with the values
*in the color array from the index given(excluded) to its end and return the final result
*/
function make_guess($index,$color_array,$guess_array)
{
	$guess = 1;
	for($i=$index+1;$i<count($color_array);$i++)
	{
		$guess*=$color_array[$i];
	}
	for($i=0;$i<count($guess_array);$i++)
	{
		$guess*=$guess_array[$i];
	}
	return $guess;
}
$num_color_array=array();//1 for white and -1 for black
$guess_array=array();
$handle=fopen ("php://stdin","r");
$color=fgets($handle);
for ($i=0;$color!="\n";$i++)
{
    if ($color=="white\n")
    {
    	array_push($num_color_array,1);
    }
    elseif ($color=="black\n")
    {
    	array_push($num_color_array,-1);
    }
    else
    {
    	$i--;//return to last index if the input is not black or white 
    	echo "Please try again\n";
    }
    $color = fgets($handle);
}
//calculate the guess of last player by multiplying all the value in num array from index 1
$first_guess=1;
for ($i=1;$i<count($num_color_array);$i++)
{
	$first_guess*=$num_color_array[$i];
}
array_push($guess_array,$first_guess);	
//call the guess function for the rest of the player (last one exclude)
for ($i=1;$i<count($num_color_array);$i++)
{
	array_push($guess_array,make_guess($i,$num_color_array,$guess_array));
}
result($num_color_array,$guess_array);