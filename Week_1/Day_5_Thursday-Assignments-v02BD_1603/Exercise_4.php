<?php
function recursive_ls($path,$indent){
	//scandirr gets the file in path as array array_diff remove the . and ..
	$files = array_diff( scandir($path), array(".", "..") );
	//iterate over the filearray
	foreach ($files as $file) {
		//print the file name and the indentation
		echo $indent,$file."\n";
    	//if the file is actually a directory
    	if (is_dir($path . '/' . $file) && is_readable($path . '/' . $file)) {
    		$indent="  ".$indent;#incriment indent by double space
    		//recursively call the function with the directory path as 
    		//new path and the newly incrimented indent variable
    		recursive_ls($path . '/' . $file,$indent);
    		//remove a double space from indent after the recursion 
    		//round has been completed
    		$indent=substr ( $indent , 2 ,strlen($indent)-2 );
		}   
    }
}

//check if the script gets a propper input
$input=getopt("i: "); 
if(!empty($input['i'])){
    $path=$input['i'];
    if(is_dir($path)){
        recursive_ls($path,"");
    }
    else {
        echo"cannot access dd: No such file or directory\n";
    }
}

