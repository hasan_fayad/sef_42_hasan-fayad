#!/bin/bash          
echo "enter threshhold 1(disk)"
read thresh1
echo "enter threshhold 2(memory)"
read thresh2
index=0
error=0
diskarray=($(df | grep -v "Filesystem" | awk {'print $1" "'})) 
percentarray=$(df | grep -v "Filesystem" | awk {'print $5'} | sed 's/%//g')
for element in $percentarray
do
	if [ $element -ge $thresh1 ]
		then
		echo "ALARM: ${diskarray[index]} is at $element %"
		((error++))
	fi
	((index++))
done
mem=$(free | grep Mem | awk {'print int($3*100/$2)'})
if [ $mem -gt $thresh2 ]
	then
	echo "ALARM: Virtual Memory is at $mem %"
	error=$((error+1))
fi
if [ $error -eq 0 ]
	then
	echo "Everything is ok"
fi
