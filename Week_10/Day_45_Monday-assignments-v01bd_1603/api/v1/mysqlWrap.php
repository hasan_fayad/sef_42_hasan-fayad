<?php

class MySQLWrap {
  
  private $dbCon ;
  private $query ;
  
  function connect($host, $user, $pass, $db) {
    $this->dbCon = new mysqli($host, $user, $pass, $db);
    if ($this->dbCon->connect_error) {
      return 0;
    } else {
      return 1;
    }
  }

  function exec_query($query) {
    $row_array = array(array());
    $result = $this->dbCon->query($query);
    if(!$result) {
      return "error";
    } else {
      $rows = $result->num_rows;
      for ($i = 0; $i < $rows; $i++) {
        $result->data_seek($i);
        $row_array[$i] = $result->fetch_array(MYSQLI_ASSOC);
      }
      $result->close();
      return json_encode($row_array, JSON_PRETTY_PRINT);
    }
  }
  
}
