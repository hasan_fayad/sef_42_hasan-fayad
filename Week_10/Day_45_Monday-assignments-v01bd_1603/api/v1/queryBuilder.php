<?php

require_once('mysqlWrap.php');

require_once('config.php');

class queryBuilder {

  private $tables = array(
    'actor' => array(
      'id' => 'actor_id',
      'fname' => 'first_name',
      'lname' => 'last_name',),
    'film' => array(
      'id' => 'film_id',
      'title' => 'title',
      'desc' => 'description',
      'year' => 'release_year',
      'lang' => 'language_id',
      'org_lang' => 'original_language_id',
      'rent_dur' => 'rental_duration',
      'rent_rate' => 'rental_rate',
      'len' => 'length',
      'cost' => 'replacement_cost',
      'rating' => 'rating',
      's_f' => 'special_features'),
    'category' => array(
      'id' => 'category_id',
      'name' => 'name'),
    'film_actor' => array(
      'fid' => 'film_id',
      'aid' => 'actor_id'),
    'film_category' => array(
      'fid' => 'film_id',
      'cid' => 'category_id')
    );
  private $map = array(
    'actor' => array(
      'film' => 'film_actor',
      'film_actor' => 'actor_id',
      'category' => 'film_actor'),
    'film_actor' => array(
      'film' => 'film_id',
      'actor' => 'actor_id',
      'category' => 'film'),
    'film' => array(
      'actor' => 'film_actor',
      'film_actor' => 'film_id',
      'category' => 'film_category',
      'film_category' => 'film_id'),
    'film_category' => array(
      'film' => 'film_id',
      'actor' => 'film',
      'category' => 'category_id'),
    'category' => array(
      'film' => 'film_category',
      'film_category' => 'category_id',
      'actor' => 'film_category')  
    );
  private $uri_array = array();
  private $query;
  private $uri;
  private $table;
  private $auxiliary  = array();

  function __construct($uri) {
    $this->uri = explode("/", $uri, 4)[3];
  }

  public function prep_uri() {
    $uri_array = explode("/", $this->uri);
    $new_array = array();
    $index = 0;
    foreach ($uri_array as $elt) {
      if(isset($this->tables[$elt])) {
        $new_array[$elt] = array();
        $index = $elt;
      } else {
        array_push($new_array[$index], $elt);
      }
    } 
    $this->uri_array = $new_array;
    return $this;
  }

  public function get_table_name() {
    $temp = $this->uri_array;
    reset($temp);
    $start = key($temp);
    $this->table = $start;
    return $this;
  }

  public function get_auxiliary() {
    $auxiliary = $uri_array = explode("/?", $this->uri);
    $this->uri = $auxiliary[0];
    if(isset($auxiliary[1])) {
      $temp_array = explode("&", $auxiliary[1]);
      foreach ($temp_array as $key => $value) {
        $this->auxiliary[explode("=", $value)[0]] = explode("=", $value)[1];
      }
    } 
    return $this;
  }

  public function build_query_from_data($data) {
    $data_array = array();
    $temp_array = explode("&", $data);
    $att = "(";
    $val = "(";
    foreach ($temp_array as $key => $value) {
      $att .= $this->atrribute_name($this->table, explode("=", $value)[0]).",";
      $val .= explode("=", $value)[1].",";
    }
    $att[strlen($att) - 1] = ")";
    $val[strlen($val) - 1] = ")";
    $this->query = "INSERT INTO {$this->table} {$att} VALUES {$val}";
    return $this;
  }

  public function build_update_query($data) {
    $temp_array = explode("&", $data);
    foreach ($temp_array as $key => $value) {
      $sub_query .= $this->atrribute_name($this->table, explode("=", $value)[0])."=".
                   explode("=", $value)[1].","; 
    }
    $sub_query[strlen($sub_query) - 1] = "";
    $this->query = "UPDATE {$this->table} SET {$sub_query} ";
    return $this;
  }

  public function build_delete_query() {
    $this->query = "DELETE FROM {$this->table} ";
    return $this;
  }

  public function build_query_head() {
    $temp = $this->uri_array;
    reset($temp);
    $table_wanted = key($temp);
    $this->query = "SELECT ";
    if(count(reset($temp))) {
      foreach (reset($temp) as $key => $value) {
        $this->query .= "{$table_wanted}.{$this->tables[$table_wanted][$value]}, ";
      }
    } else {
      $this->query .= "{$table_wanted}.*, ";
    }
    $this->query = substr($this->query, 0, -2);
    $this->query .= " FROM {$table_wanted}";
    return $this;
  }
  
  public function build_query_body() {
    $temp = $this->uri_array;
    reset($temp);
    $start = key($temp);
    unset($temp[key($temp)]);
    $this->inner_join($temp, $start);
    return $this;
  }

  public function inner_join($uri_array, $start) {
    $first = $start;
    $next = "";
    foreach ($uri_array as $key => $value) {
      $end = $key;
      while($first != $end) {
        $next = $this->map[$first][$end];
        if(!isset($this->tables[$next])) {
          $this->query .= "\n INNER JOIN {$end} ON {$first}.{$next}={$end}.{$next}";
          $first = $end;
        } else {
          $this->query .= "\n INNER JOIN {$next} ON {$first}.{$this->map[$first][$next]}={$next}.{$this->map[$first][$next]}";
          $first = $next;   
        }
      }
      for($i = 0; $i < count($value); $i += 2){
        $this->query .= "\n AND {$end}.{$this->tables[$end][$value[$i]]}='{$value[$i+1]}' "; 
      }
    }
  }

  public function add_auxiliary() { 
    if(count($this->auxiliary)) {  
      $this->query .= "\nWHERE\n";
      foreach ($this->auxiliary as $key => $value) {
        $this->query .= " {$this->table}.{$this->atrribute_name($this->table, $key)}=\"{$value}\"\n";
        $this->query .= "AND";
      }
      $this->query = substr($this->query, 0, -3);
    }
    return $this;
  }

  public function get() {
    return $this->query;
  }

  public function atrribute_name($table, $alias) {
    if(isset($this->tables[$table][$alias])) {
      return $this->tables[$table][$alias];        
    }
  }
}
