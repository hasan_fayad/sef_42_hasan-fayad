<?php

require_once('mysqlWrap.php');

require_once('config.php');

require_once('queryBuilder.php');

$Query = new queryBuilder( urldecode($_SERVER['REQUEST_URI']));
//$query = $Query->prep_uri()->build_query_head()->build_query_body()->get();
$sqlWrap = new MySQLWrap();
$sqlWrap->connect($host, $user, $pass, $db);
//$json = $sqlWrap->exec_query($c);
$method = $_SERVER['REQUEST_METHOD'];
$request = explode("/", substr(@$_SERVER['PATH_INFO'], 1));
$uri = explode("/",$_SERVER['REQUEST_URI']);
//echo $method."--->".file_get_contents("php://input")."-->".$_SERVER['REQUEST_URI']."\n";
switch ($method) {
  case 'POST':
  	$query = $Query->prep_uri()->get_table_name()->build_query_from_data(file_get_contents("php://input"))->get();
	echo $query;
    break;
  case 'GET':
    $query = $Query->get_auxiliary()->prep_uri()->get_table_name()->build_query_head()->build_query_body()->add_auxiliary()->get();
    echo $query;
    echo $json = $sqlWrap->exec_query($query);
    break;
  case 'UPDATE':
	$query = $Query->get_auxiliary()->prep_uri()->get_table_name()->build_update_query(file_get_contents("php://input"))->add_auxiliary()->get();   
	echo $query;    
    break;
  case 'DELETE':
	$query = $Query->get_auxiliary()->prep_uri()->get_table_name()->build_delete_query()->add_auxiliary()->get();   
	echo $query;
    break;
  default:
   // print_r($request);
    break;
}
