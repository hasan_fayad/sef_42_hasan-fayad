<html>
	<head></head>
	<body>
		<p>
			to use the api start by finding wich table you want to use ex:you want actors your uri should be 127.0.0.1/api/v1/actor
			<br>
			if you want specific fields ex first name and id your uri becomes 127.0.0.1/api/v1/actor/id/fname
			<br>
			if you need to specify a specific movie (all actor in this movie ) the uri becomes 127.0.0.1/api/v1/actor/id/fname/film/title/your_film_title  <br>
			if you want to specify  condition on actor ex name ="actor" and id =1 just chain it to uri 
			ex  127.0.0.1/api/v1/actor/id/fname/film/title/your_film_name/?id=1&fname=actor<br>
			the same works for films and category
			<br><br>
			'actor' => 
		      'id',
		      'fname',
		      'lname'
		    <br>
		    'film' =>
		      'id',
		      'title',
		      'desc',
		      'year',
		      'lang' ,
		      'org_lang',
		      'rent_dur',
		      'rent_rate',
		      'len',
		      'cost',
		      'rating',
		      's_f'
		    <br>
		    'category' =>
		      'id',
		      'name'
		    <br>
		    'film_actor' =>
		      'fid',
		      'aid'
		    <br>
		    'film_category' => 
		      'fid',
		      'cid'
		    <br>
			to update, delete or insert use UPDATE,DELETE or POST request with the data you want to insert according to previous table attributes ex (insert): POST  fid=1&cid=1 to uri  127.0.0.1/api/v1/film_category 
		</p> 
	</body>
</html>	