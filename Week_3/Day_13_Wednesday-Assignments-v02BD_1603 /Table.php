<?php
require_once("Trie.php");
class Table
{
  private $trie;
  private $table_name;
  private $db_name;
  private $nb_columns;
  private $table_handler;
  private $search_array=array();
  
  function set_name($name)
  {
    $this->table_name=$name;
  }

  function create_table($count,$table_name,$db_name)
  {
    $this->trie=new Trie();
    $this->db_name=$db_name;
    $this->nb_columns=$count;
    $this->table_name=$table_name;
    $this->table_handler=fopen($this->db_name.DIRECTORY_SEPARATOR.$this->table_name,'c+');
    $trie_handler=fopen($this->db_name.DIRECTORY_SEPARATOR.$this->table_name."trie",'c+');
    $this->trie->set_name($table_name,$db_name,$trie_handler);
  }
  
  function delete_table()
  {
    if(file_exists($this->db_name.DIRECTORY_SEPARATOR.$this->table_name))
    {
      unlink($this->db_name.DIRECTORY_SEPARATOR.$this->table_name);
      unlink($this->db_name.DIRECTORY_SEPARATOR.$this->table_name."trie");
    }
    $this->table_name="";
  }
  
  function get_table_name()
  {
    return $this->table_name;
  }
  
  function add_record($record_array)
  {
    $pointer="";
    $line="";
    fseek($this->table_handler,0,SEEK_END);
    $file_index=ftell($this->table_handler);
    foreach($record_array as $key=>$record)
    {
      $index_result=$this->trie->index_record($record,$file_index,(!$key));
      if($key==0&&$index_result==0)
      {
        echo "duplicate key\n";
        return 0;
      }
      $pointer.=str_pad($index_result,10);
      $line.=",".$record;
    }
    fwrite($this->table_handler,$pointer.$line."\n");
  }
  
  function get($record)
  {
    $result=$this->trie->get_record($record);
    $matching_array=array();
    if($result['found']==0)
    {
      return 0;
    }
    unset($result['found']);
    foreach($result as $pointer)
    {
      $this->recursive_find($record,$pointer);
    }
    foreach($this->search_array as $result)
    {
      printf("%s",$result);
    }
    $this->search_array=array();
  }

  function recursive_find($record,$pointer)
  {
    $next=1;
    fseek($this->table_handler,$pointer);
    $line=fgets($this->table_handler);
    $line_array=explode(",",$line);
    for($i=1;$i<$this->nb_columns+1;$i++)
    {
      if($line_array[$i]==$record)
      {
        $this->search_array[$line_array[1]]=substr($line,1+$this->nb_columns*10,strlen($line));
        $next=intval(substr($line,($i-1)*10,10));
      }
      if($next!=1)
      {
        $this->recursive_find($record,$next);
      }
    }
  }

  function delete_key($key)
  {
    $result=$this->trie->search_trie(str_split(crc32($key)));
    if($result['found']==1)
    {
      $this->trie->trie[$result['index']][11]="deleted";
      $this->trie->edit_trie_file($result['index'],11);
      unset($this->trie->trie[$result['index']][11]);

    }   
  }
}


