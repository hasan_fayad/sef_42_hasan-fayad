<?php

class Query
{
  private $table;
  private $database;
  
  function __construct($database,$table)
  {
    $this->database=$database;
    $this->table=$table;
  }
  
  function analyse_query($query)
  {
    $is_database=(!empty($this->database->get_db_name()));
    $is_table=(!empty($this->table->get_table_name()));
    $query_array=explode(",",$query);
    if(count($query_array)>1)
    {
      switch ($query_array[0])
      {
        case "CREATE":
          if($query_array[1]=="DATABASE"&&count($query_array)==3)
          {
            $this->database->create_db($query_array[2]);
            $this->table->set_name("");
          }
          if($query_array[1]=="TABLE"&&count($query_array)>4&&$is_database)
          {
            array_splice($query_array,0,2);
            array_splice($query_array,1,1);
            $this->table->create_table(count($query_array)-1,$query_array[0],$this->database->get_db_name());
          }
          break;
        case "ADD":
          if($is_table&&$is_database)
          {
            array_splice($query_array,0,1);
            $this->table->add_record($query_array);
          }
          break;
        case "GET":
          if(count($query_array)==2&&$is_table&&$is_database)
          {
            array_splice($query_array,0,1);
            $this->table->get_record($query_array);
          }
          break;
        case "DELETE":
          if(count($query_array)==3&&$query[1]=="ROW")
          $this->table->delete_key($query[2]);
          else
          {
            $this->database->delete_db();
          }
          break;
        default:
          echo "wrong format , please try again \n";
          
      }
    }
  }
  
  function set_query($query)
  {
    $this->query=$query;
  }
}
