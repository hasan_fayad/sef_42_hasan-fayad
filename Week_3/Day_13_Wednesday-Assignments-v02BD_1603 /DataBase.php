<?php

class DataBase
{
  private $db_name;

  
  function create_db($db_name)
  {
    $this->db_name=$db_name;
    if(!is_dir($this->db_name))
    {
      mkdir($this->db_name);
    }
  }
  
  function delete_db()
  {
    if(is_dir($this->db_name))
    {
      foreach (scandir($this->db_name) as $item)
      {
        if ($item == '.' || $item == '..')
        {
        continue;
        }
        unlink($this->db_name.DIRECTORY_SEPARATOR.$item);
      }
    rmdir($this->db_name);
    $this->db_name="";
    }
  }
  
  function get_db_name()
  {
    return $this->db_name;
  }
}
