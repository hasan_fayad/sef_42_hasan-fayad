<?php

class Trie
{
  private $table_name="test";
  private $db_name="test";
  private $trie=array(array());
  private $trie_handler;
    
  function set_name($table_name,$db_name,$trie_handler)
  {
    $this->table_name=$table_name;
    $this->db_name=$db_name;
    $this->trie_handler=$trie_handler;
  }
   
  function index_record($record,$file_index,$prim_key)
  {
    $index_result=1;
    $hash=crc32($record);
    $hash_array=str_split($hash);
    $result=$this->search_trie($hash_array);
    $index=$result['index'];
    $key=$result['key'];
    if($result['found'])
    {
      $this->fetch_trie_file($result['index'],10+$prim_key);
      if(isset($this->trie[$result['index']][10+$prim_key]))
      {
        if($prim_key)
        {
          return 0;
        }
        $index_result=$this->trie[$result['index']][10+$prim_key];
      }
      $this->trie[$result['index']][10+$prim_key]=$file_index;
      $this->edit_trie_file($result['index'],10+$prim_key);
      return $index_result;
    }
    else
    {
      fseek($this->trie_handler,0,SEEK_END);
      $count=round(ftell($this->trie_handler)/120);
      $count+=($count==$index);
      $flag=0;//echo $key."jjjjjjjjj";
      for($i=$key;$i<count($hash_array);$i++)
      {
      
      $this->trie[$index][$hash_array[$i]]=$count;
      $this->edit_trie_file($index,$hash_array[$i]);
      $index=$count;
      $count++;
      }
      $this->trie[$index][10+$prim_key]=$file_index;
      $this->edit_trie_file($index,10+$prim_key);
    }
    return $index_result;
  }
  
  function search_trie($hash_array)
  {
    $result=array('index'=>0,'found'=>1,'key'=>0);
    $i=0;
    foreach($hash_array as $key=>$digit)
    {
      $this->fetch_trie_file($i,$digit);
      if(!isset($this->trie[$i][$digit]))
      {
        $result['found']=0;
        break;
      }
      $i=$this->trie[$i][$digit];
      $result['index']=$i;
    }
    $result['key']=$key;
    return $result;
  }
  
  function edit_trie_file($index,$key)
  {
    $line=$this->trie[$index][$key];
    $line=str_pad($line,10);
    fseek($this->trie_handler,(($index*12)+$key)*10);
    fwrite($this->trie_handler,$line);
  }
  
  function fetch_trie_file($index,$key)
  {
    if(!isset($this->trie[$index][$key]))
    {
      fseek($this->trie_handler,(($index*12)+$key)*10);
      $value=fread($this->trie_handler,10);
      if(isset($value[0])&&is_numeric($value[0]))
      {
        $this->trie[$index][$key]=intval($value);
        return 1;
      }
      return 0;
    }
    return 1;
  }
  
  function get_record($record)
  {
    $result=array('elmnt'=>0,'key'=>0,'found'=>1);
    $hash=crc32($record);
    $hash_array=str_split($hash);
    $search_result=$this->search_trie($hash_array);
    if($result['found']==0)
    {
      $result['found']=0;
      return $result;
    }
    if($this->fetch_trie_file($search_result['index'],10))
    {
      $result['elmnt']=$this->trie[$search_result['index']][10];
    }
    if($this->fetch_trie_file($search_result['index'],11))
    {
      $result['key']=$this->trie[$search_result['index']][11];
    }
    return $result;
  }
  
}
