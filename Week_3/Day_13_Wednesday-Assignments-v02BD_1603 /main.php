<?php

require_once("DataBase.php");
require_once("Query.php");
require_once("Table.php");
require_once("Trie.php");


$database=new DataBase();
$table=new Table();
$query=new Query($database,$table);
$input="";
while($input!="exit")
{
  $input=readline();
  trim($input,"\n");
  $query->analyse_query($input);
}